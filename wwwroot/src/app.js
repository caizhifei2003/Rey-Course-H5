(function () {
    'use strict';

    angular
        .module('app', [
            //! angular
            'ngResource',
            'ngAnimate',
            'ngSanitize',

            //! third party.
            'ui.router'
        ]);
})();