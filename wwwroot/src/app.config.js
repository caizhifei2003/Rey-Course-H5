(function () {
    'use strict';

    angular
        .module('app')
        .config(function ($stateProvider) {
            var aboutState = {
                name: 'about',
                url: '/about',
                template: '<h3>Its the UI-Router hello world app!</h3>'
            }

            $stateProvider.state({ name: 'home', url: '/home', templateUrl: '/templates/home.html' });
            $stateProvider.state(aboutState);
        });
})();